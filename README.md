**Social Media App**

A proof of concept app that is inspired by Twitter. Created using SwiftUI and Firebase.
 
---

## Features

- Login and or Register with an email, username, full name, password, and a profile picture
- Start a conversation with a registered user
- Search functionality when looking for a user to chat with
- Reply to a conversation
- Automatically syncs the conversation
- Post a tweet
- Like/Unlike a tweet
- Follow a User
