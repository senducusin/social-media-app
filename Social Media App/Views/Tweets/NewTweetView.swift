//
//  NewTweetView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI
import Kingfisher

struct NewTweetView: View {
    @Binding var isPresented: Bool
    @ObservedObject var viewModel: UploadTweetViewModel
    
    init(isPresented: Binding<Bool>){
        self._isPresented = isPresented
        viewModel = UploadTweetViewModel(isPresented: isPresented)
    }
    
    var body: some View {
        NavigationView{
            VStack {
                HStack(alignment: .top) {
                    KFImage(AuthService.shared.userVm?.profileImageUrl)
                        .resizable()
                        .scaledToFill()
                        .clipped()
                        .frame(width: 64, height: 64)
                        .clipShape(Circle())
                    
                    TextArea("What's happening?", textBinding:$viewModel.captionText)
                    
                    Spacer()
                }
                .padding()
                .navigationBarItems(leading:
                                        Button(action: {
                                            isPresented.toggle()
                                        }, label: {
                                            Text("Cancel")
                                        }),
                                    trailing:
                                        Button(action: {
                                            viewModel.uploadTweet()
                                        }, label: {
                                            Text("Tweet")
                                                .padding(.horizontal)
                                                .padding(.vertical, 8)
                                                .background(Color.blue)
                                                .foregroundColor(.white)
                                                .clipShape(Capsule())
                                        })
                )
                Spacer()
            }
        }
    }
}

struct NewTweetView_Previews: PreviewProvider {
    static var previews: some View {
        NewTweetView(isPresented: .constant(true))
    }
}
