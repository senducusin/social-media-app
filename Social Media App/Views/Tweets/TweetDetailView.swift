//
//  TweetDetailView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/27/21.
//

import SwiftUI
import Kingfisher

struct TweetDetailView: View {
    
    let tweetVm: TweetViewModel
    
    var body: some View {
        VStack(alignment:.leading, spacing: 16) {
            HStack {
                KFImage(tweetVm.uploaderProfileImageUrl)
                    .resizable()
                    .scaledToFill()
                    .clipped()
                    .frame(width: 56, height: 56)
                    .clipShape(Circle())
            
                VStack(alignment: .leading, spacing:4) {
                    Text(tweetVm.uploaderFullname)
                        .font(.system(size: 14, weight: .semibold))
                    
                    Text("@\(tweetVm.uploaderUsername)")
                        .font(.system(size: 14))
                        .foregroundColor(.gray)
                }
            }
            
            Text(tweetVm.caption)
                .font(.system(size: 22))
            
            Text(tweetVm.detailedTimestampString)
                .font(.system(size: 14))
                .foregroundColor(.gray)
            
            Divider()
            
            HStack(spacing:12) {
                HStack(spacing:4){
                    Text("0")
                        .font(.system(size: 14, weight: .semibold))
                    
                    Text("Retweets")
                        .font(.system(size: 14))
                        .foregroundColor(.gray)
                }
                
                HStack(spacing:4){
                    Text("0")
                        .font(.system(size: 14, weight: .semibold))
                    
                    Text("Likes")
                        .font(.system(size: 14))
                        .foregroundColor(.gray)
                }
            }
            
            Divider()
            
            TweetAction(tweetVm: tweetVm)
            
            Spacer()
        }
        .padding()
    }
}
