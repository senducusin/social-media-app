//
//  FeedCell.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI
import Kingfisher

struct FeedCell: View {
    let tweetVm: TweetViewModel
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20){
            HStack(alignment: .top, spacing: 12){
                
                // MARK: - Thumbnail
                KFImage(tweetVm.uploaderProfileImageUrl)
                    .resizable()
                    .scaledToFill()
                    .clipped()
                    .frame(width:56, height: 56)
                    .clipShape(Circle())
                    .padding(.leading)
                
                VStack(alignment: .leading){
                    
                    // MARK: - Cell Header
                    HStack {
                        Text(tweetVm.uploaderFullname)
                            .font(.system(size: 14, weight: .semibold))
                            .foregroundColor(.black)
                        
                        Text("@\(tweetVm.uploaderUsername) •")
                            .font(.system(size: 14, weight: .semibold))
                            .foregroundColor(.gray)
                        
                        Text(tweetVm.timestampString)
                            .font(.system(size: 14, weight: .semibold))
                            .foregroundColor(.gray)
                    }
                    
                    // MARK: - Cell Message
                    Text(tweetVm.caption)
                        .foregroundColor(.black)
                }
            }
            .padding(.trailing)
            
            // MARK: - Controls
            TweetAction(tweetVm: tweetVm)
        }
        .padding(.bottom)
        .padding(.leading, -16)
    }
}
