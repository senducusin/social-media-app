//
//  TweetAction.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/27/21.
//

import SwiftUI

struct TweetAction: View {
    @ObservedObject var viewModel: TweetActionViewModel
    
    init(tweetVm: TweetViewModel){
        viewModel = TweetActionViewModel(tweetVm: tweetVm)
    }
    
    var body: some View {
        VStack(spacing:20){
            HStack{
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Image(systemName: "bubble.left")
                        .font(.system(size: 16))
                        .frame(width: 32, height: 32)
                })
                
                Spacer()
                
                Button(action: {}, label: {
                    Image(systemName: "arrow.2.squarepath")
                        .font(.system(size: 16))
                        .frame(width: 32, height: 32)
                })
                
                Spacer()
                
                Button(action: {
                    viewModel.likeUnlikeHandler()
                }, label: {
                    Image(systemName: viewModel.likeIconImage)
                        .font(.system(size: 16))
                        .frame(width: 32, height: 32)
                        .foregroundColor(viewModel.likeIconColor)
                })
                
                Spacer()
                
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Image(systemName: "bookmark")
                        .font(.system(size: 16))
                        .frame(width: 32, height: 32)
                })
            }
            .foregroundColor(.gray)
            .padding(.horizontal)
            
            Divider()
        }
    }
}
