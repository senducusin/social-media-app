//
//  FeedView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

struct FeedView: View {
    @State var isShowingNewTweetView = false
    @ObservedObject var viewModel = FeedViewModel()
    
    var body: some View {
        ZStack(alignment: .bottomTrailing){
            
            // MARK: - Scroll View
            ScrollView{
                LazyVStack{
                    ForEach(viewModel.tweetVms) { tweetVm in
                        NavigationLink(destination: TweetDetailView(tweetVm: tweetVm)) {
                            FeedCell(tweetVm: tweetVm)
                        }
                        
                    }
                }.padding()
            }
            
            // MARK: - Floating Button
            Button(action: {
                isShowingNewTweetView.toggle()
            }, label: {
                Image("tweet")
                    .resizable()
                    .renderingMode(.template)
                    .frame(width: 32, height: 32)
                    .padding()
            })
            .background(Color(.systemBlue))
            .foregroundColor(.white)
            .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            .padding()
            .fullScreenCover(isPresented: $isShowingNewTweetView, onDismiss: {
                viewModel.fetchTweets()
            }, content: {
                NewTweetView(isPresented: $isShowingNewTweetView)
            })
        }
    }
}

struct FeedView_Previews: PreviewProvider {
    static var previews: some View {
        FeedView()
    }
}
