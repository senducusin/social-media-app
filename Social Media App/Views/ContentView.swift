//
//  ContentView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI
import Kingfisher

struct ContentView: View {
    
    @EnvironmentObject var authViewModel: AuthViewModel
    @ObservedObject var viewModel = TabMenuViewModel()
    
    var body: some View {
        
        Group {
            if authViewModel.userSession != nil {
                NavigationView {
                    
                    TabView(selection: $viewModel.selectedIndex){
                        // MARK: - Feed
                        FeedView()
                            .tabItem {
                                Image(systemName: "house")
                                Text("Home")
                            }.tag(0)
                        
                        // MARK: - Search
                        SearchView()
                            .tabItem {
                                Image(systemName: "magnifyingglass")
                                Text("Search")
                            }.tag(1)
                        
                        // MARK: - Message
                        ConversationView()
                            .tabItem {
                                Image(systemName: "envelope")
                                Text("Messages")
                            }.tag(2)
                    }
                    
                    
                    // MARK: - NavigationView Modifiers
                    .navigationBarTitle(viewModel.titleBar)
                    .navigationBarItems(leading: Button(action: {
                        authViewModel.signOut()
                    }, label: {
                        KFImage(authViewModel.user?.profileImageUrl)
                            .resizable()
                            .scaledToFill()
                            .clipped()
                            .frame(width: 32, height: 32)
                            .cornerRadius(16)
                    }))
                    .navigationBarTitleDisplayMode(.inline)
                }
            }else{
                LoginView()
            }
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
