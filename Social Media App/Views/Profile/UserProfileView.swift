//
//  UserProfileView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI

struct UserProfileView: View {
    
    @ObservedObject var viewModel: UserProfileViewModel
    
    init(user: SocialMediaUser){
        viewModel = UserProfileViewModel(user: user)
    }
    
    var body: some View {
        ScrollView {
            VStack {
                ProfileHeader(viewModel: viewModel,
                              isFollowed: $viewModel.isFollowed)
                
                ProfileFilterButton(selectedOption: $viewModel.selectedFilter)
                    .padding()
                
                VStack{
                    ForEach(viewModel.tweetsToShow){ tweetVm in
                        FeedCell(tweetVm: tweetVm)
                            .padding()
                    }
                }.padding(.horizontal, 30)
                
                Spacer()
                
            }.padding()
        }
        
        .navigationTitle(viewModel.userVM.username)
    }
}
