//
//  ProfileHeader.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI
import Kingfisher

struct ProfileHeader: View {
    
    let viewModel: UserProfileViewModel
    @Binding var isFollowed: Bool
    
    var body: some View {
        VStack{
            KFImage(viewModel.userVM.profileImageUrl)
                .resizable()
                .scaledToFill()
                .clipped()
                .frame(width:120, height:120)
                .clipShape(Circle())
                .shadow(color:.black, radius: 6)
            
            Text(viewModel.userVM.fullname)
                .font(.system(size: 16, weight: .semibold))
                .padding(.top,8)
            
            Text("@\(viewModel.userVM.username)")
                .font(.subheadline)
                .foregroundColor(.gray)
            
            Text(viewModel.userVM.description)
                .font(.system(size: 14))
                .padding(.top,8)
                .foregroundColor(.gray)
            
            HStack(spacing: 40) {
                VStack {
                    Text(viewModel.userVM.followers)
                        .font(.system(size: 16))
                        .bold()
                    
                    Text("Followers")
                        .font(.footnote)
                        .foregroundColor(.gray)
                }
                
                VStack {
                    Text(viewModel.userVM.following)
                        .font(.system(size: 16))
                        .bold()
                    
                    Text("Following")
                        .font(.footnote)
                        .foregroundColor(.gray)
                }
            }.padding()
            
            ProfileActionButton(isFollowed: $isFollowed, viewModel: viewModel)
        }
    }
}
