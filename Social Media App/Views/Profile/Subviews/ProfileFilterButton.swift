//
//  ProfileFilterButton.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI

enum ProfileFilterOptions: Int, CaseIterable {
    case tweets, replies, likes
    
    var title: String {
        switch self {
        
        case .tweets:
            return "Tweets"
        case .replies:
            return "Tweets & Replies"
        case .likes:
            return "Likes"
        }
    }
}

struct ProfileFilterButton: View {
    
    @Binding var selectedOption: ProfileFilterOptions
    
    private let underlineWidth = UIScreen.main.bounds.width / CGFloat(ProfileFilterOptions.allCases.count)
    
    private var padding: CGFloat {
        let rawValue = CGFloat(selectedOption.rawValue)
        let count = CGFloat(ProfileFilterOptions.allCases.count)
        return ((UIScreen.main.bounds.width / count) * rawValue) + 16
    }
    
    var body: some View {
        VStack(alignment: .leading){
            HStack {
                ForEach(ProfileFilterOptions.allCases, id: \.self) { option in
                    Button(action:{
                        
                        selectedOption = option
                        
                    }, label:{
                        Text(option.title)
                            .frame(width: underlineWidth - 8)
                            .foregroundColor(.blue)
                    })
                }
            }
            
            Rectangle()
                .frame(width: underlineWidth - 32, height: 3, alignment: .center)
                .foregroundColor(.blue)
                .padding(.leading, padding)
                .animation(.spring())
        }
    }
}

struct FilterButton_Previews: PreviewProvider {
    static var previews: some View {
        ProfileFilterButton( selectedOption: .constant(.tweets))
    }
}
