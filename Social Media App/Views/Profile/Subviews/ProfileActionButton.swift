//
//  ProfileActionButton.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI

struct ProfileActionButton: View {
    @Binding var isFollowed: Bool
    let viewModel: UserProfileViewModel
    
    var body: some View {
        
        if viewModel.userVM.isCurrentUser {
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                Text("Edit Profile")
                    .frame(width: 360, height: 40)
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(20, antialiased: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
            })
        }else{
            HStack {
                Button(action: {
                    viewModel.followHandler()
                }, label: {
                    Text(viewModel.followLabel)
                        .frame(width: 180, height: 40)
                        .background(Color.blue)
                        .foregroundColor(.white)
                        .cornerRadius(20, antialiased: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                })
                
                NavigationLink(
                    destination: ChatView(recepientUserVm: viewModel.userVM),
                    label: {
                        Text("Message")
                            .frame(width: 180, height: 40)
                            .background(Color.purple)
                            .foregroundColor(.white)
                            .cornerRadius(20, antialiased: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
                    })
            }
        }
        
    }
}
