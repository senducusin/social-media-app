//
//  SearchCell.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/22/21.
//

import SwiftUI
import Kingfisher

struct SearchCell: View {
    let viewModel: UserViewModel
    
    init(user: SocialMediaUser){
        viewModel = UserViewModel(user: user)
    }
    
    var body: some View {
        HStack(spacing:12) {
            KFImage(viewModel.profileImageUrl)
                .resizable()
                .scaledToFill()
                .clipped()
                .frame(width:56, height:56)
                .clipShape(Circle())
            
            VStack(alignment: .leading, spacing: 4){
                Text(viewModel.username)
                    .font(.system(size: 14, weight: .semibold))
                
                Text(viewModel.fullname)
                    .font(.system(size: 14))
            }
            .foregroundColor(.black)
            
            Spacer()
        }
    }
}
