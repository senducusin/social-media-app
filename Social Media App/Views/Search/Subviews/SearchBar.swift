//
//  SearchBar.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

struct SearchBar: View {
    @Binding var searchKeyword: String
    
    var body: some View {
        HStack {
            TextField("Search...", text: $searchKeyword)
                .padding(8)
                .padding(.horizontal, 24)
                .background(Color(.systemGray6))
                .cornerRadius(8)
                .overlay(
                    
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.gray)
                            .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 8)
                    }
                    
                )
        }
        .padding(.horizontal, 10)
        
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBar(searchKeyword: .constant(""))
    }
}

