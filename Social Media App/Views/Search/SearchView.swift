//
//  SearchView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI

struct SearchView: View {
    @ObservedObject var viewModel = SearchViewModel(config: .search)
    
    var body: some View {
        VStack{
            
            SearchBar(searchKeyword: $viewModel.searchText)
                .padding()
            
            ScrollView{
                VStack(alignment:.leading) {
                    ForEach(viewModel.showUsers){ user in
                        HStack{ Spacer() }
                        NavigationLink(
                            destination: LazyView(UserProfileView(user: user)),
                            label: {
                                SearchCell(user: user)
                            })
                    } 
                }
                .padding(.leading)
                .frame(maxWidth:.infinity)
            }
        }
        
        .navigationTitle("Search")
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
