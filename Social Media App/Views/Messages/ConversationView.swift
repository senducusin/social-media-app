//
//  ConversationView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/22/21.
//

import SwiftUI

struct ConversationView: View {
    @ObservedObject var viewModel = ConversationViewModel()
    
    var body: some View {
        ZStack(alignment: .bottomTrailing){
            
            if let userVm = viewModel.userVm {
                NavigationLink(
                    destination: LazyView(ChatView(recepientUserVm: userVm)),
                    isActive: $viewModel.showChat,
                    label: {})
            }
            
            // MARK: - Scroll View
            ScrollView{
                VStack{
                    ForEach(viewModel.recentMessages) { conversation in
                        
                        NavigationLink(
                            destination: LazyView(ChatView(recepientUserVm: conversation.userVm)),
                            label: {
                                ConversationCell(conversation: conversation)
                            })
                        
                    }
                }
                .padding(.top)
            }
            
            // MARK: - Floating Button
            Button(action: {
                viewModel.isShowingNewMessageView.toggle()
            }, label: {
                Image(systemName: "envelope")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 32, height: 32)
                    .padding()
            })
            .background(Color(.systemBlue))
            .foregroundColor(.white)
            .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            .padding()
            .sheet(isPresented: $viewModel.isShowingNewMessageView,
                   content: {
                    NewMessageView(
                        startChat: $viewModel.isShowingNewMessageView,
                        show: $viewModel.showChat,
                        userVm: $viewModel.userVm
                    )
                   })
        }
    }
}

struct ConversationView_Previews: PreviewProvider {
    static var previews: some View {
        ConversationView()
    }
}
