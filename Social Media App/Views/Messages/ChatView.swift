//
//  ChatView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/22/21.
//

import SwiftUI

struct ChatView: View {
    
    @ObservedObject var viewModel: ChatViewModel
    
    init(recepientUserVm: UserViewModel){
        viewModel = ChatViewModel(recepientUserVm: recepientUserVm)
    }
    
    var body: some View {
        VStack {
            ScrollView{
                ScrollViewReader{ value in
                    VStack(alignment: .leading, spacing:12){
                        ForEach(viewModel.messages) { conversation  in
                            
                            ChatCell(conversation)
                        }
                    }
                    .onChange(of: viewModel.messages.count){ _ in
                        
                        if let lastMessage = viewModel.messages.last {
                            value.scrollTo(lastMessage.id, anchor: .bottom)
                        }
                    }
                }
            }
            .padding(.top)
            
            MessageInputField(
                text: $viewModel.text,
                height: $viewModel.height,
                action: viewModel.sendMessage
            )
                .padding()
                .navigationTitle(viewModel.recepientUserVm.username)
        }
    }
}
