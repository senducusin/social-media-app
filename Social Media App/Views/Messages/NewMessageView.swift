//
//  NewMessageView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI

struct NewMessageView: View {
    @Binding var startChat: Bool
    @Binding var show: Bool
    @Binding var userVm: UserViewModel?
    @ObservedObject var viewModel = SearchViewModel(config: .newMessage)
    
    var body: some View {
        VStack{
            
            SearchBar(searchKeyword: $viewModel.searchText)
                .padding()
            
            ScrollView{
                VStack(alignment:.leading) {
                    ForEach(viewModel.showUsers){ user in
                        HStack{ Spacer() }
                        Button(action: {
                            show.toggle()
                            startChat.toggle()
                            userVm = UserViewModel(user: user)
                        }, label: {
                            SearchCell(user: user)
                        })
                    }
                }
                .padding(.leading)
                .frame(maxWidth:.infinity)
            }
        }
        
        .navigationTitle("Search")
    }
}
