//
//  ConversationCell.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/22/21.
//

import SwiftUI
import Kingfisher

struct ConversationCell: View {
    
//    @ObservedObject var viewMo()
    let conversation: Conversation
    
    var body: some View {
        VStack {
            HStack(spacing:12) {
                KFImage(conversation.userVm.profileImageUrl)
                    .resizable()
                    .scaledToFill()
                    .clipped()
                    .frame(width:56, height:56)
                    .clipShape(Circle())
                
                VStack(alignment: .leading, spacing: 4){
                    Text(conversation.userVm.username)
                        .font(.system(size: 14, weight: .semibold))
                    
                    Text(conversation.message.text)
                        .font(.system(size: 14))
                        .lineLimit(2)
                }
                .frame(height:64)
                .foregroundColor(.black)
                .padding(.trailing)
                
                Spacer()
            }
            
            Divider()
        }
        .padding(.horizontal)
    }
}
