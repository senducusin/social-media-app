//
//  ChatBubble.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/22/21.
//

import SwiftUI

struct ChatBubble: Shape {
    var isFromCurrentUser: Bool
    
    func path(in rect: CGRect) -> Path {
        
        let bottomRoundedCorner: UIRectCorner = isFromCurrentUser ? .bottomLeft : .bottomRight
        
        let cornerRadii = CGSize(width: 16, height: 16)
        
        let path = UIBezierPath(
            roundedRect: rect,
            byRoundingCorners: [.topLeft, .topRight, bottomRoundedCorner],
            cornerRadii: cornerRadii
        )
        
        return Path(path.cgPath)
    }
    
}

struct ChatBubble_Previews: PreviewProvider {
    static var previews: some View {
        ChatBubble(isFromCurrentUser: true)
    }
}
