//
//  ChatCell.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI
import Kingfisher

struct ChatCell: View {
    let viewModel: ChatCellViewModel
    
    init(_ conversation: Conversation){
        print(conversation)
        viewModel = ChatCellViewModel(conversation: conversation)
    }
    
    var body: some View {
        HStack {
            
            HStack(alignment: .bottom) {
                if viewModel.isRecepient {
                    KFImage(viewModel.imageUrl)
                        .resizable()
                        .scaledToFill()
                        .frame(width:40, height: 40)
                        .clipShape(Circle())
                    
                    Text(viewModel.messageText)
                        .padding()
                        .background(viewModel.backgroundColor)
                        .clipShape(ChatBubble(isFromCurrentUser:  viewModel.useCurrentUserBubble))
                        .foregroundColor(viewModel.foregroundColor)
                        .padding(.trailing, 8)
                        
                }else{
                    Spacer()
                    
                    Text(viewModel.messageText)
                        .padding()
                        .background(viewModel.backgroundColor)
                        .clipShape(ChatBubble(isFromCurrentUser:  viewModel.useCurrentUserBubble))
                        .foregroundColor(viewModel.foregroundColor)
                        .padding(.leading, 40)
                        .padding(.trailing, 16)
                }
                
                if viewModel.isRecepient {
                    Spacer()
                }
                
            } .padding(.horizontal)
            
        }
    }
}
