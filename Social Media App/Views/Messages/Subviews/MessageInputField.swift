//
//  MessageInputView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/22/21.
//

import SwiftUI

struct MessageInputField: View {
    
    @Binding var text: String
    @Binding var height: CGFloat
    var action: () -> ()
    
    var body: some View {
        HStack{
            TextField("Message...", text: $text)
                .textFieldStyle(PlainTextFieldStyle())
                .frame(minHeight: 30)
            
//            ExpandableInputField(text: $text, height: $height)
//                .frame(height: height < 150 ? height : 150)
//                .padding(.horizontal)
//                .background(Color.white)
//                .cornerRadius(15)
            
            Button(action: action, label: {
                Text("Send")
            })
        }
    }
}
