//
//  ExpandableInputField.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 6/28/21.
//

import SwiftUI

struct ExpandableInputField: UIViewRepresentable {
    @Binding var text: String
    @Binding var height: CGFloat
    
    func makeCoordinator() -> Coordinator {
        return ExpandableInputField.Coordinator(parentView: self)
    }
    
    func makeUIView(context: Context) -> UITextView {
        let view = UITextView()
        view.isEditable = true
        view.isScrollEnabled = true
        view.text = "Enter Message"
        view.font = .systemFont(ofSize: 18)
        view.textColor = .gray
        view.backgroundColor = .clear
        view.delegate = context.coordinator
        return view
    }
    
    func updateUIView(_ uiView: UITextView, context: Context) {
        DispatchQueue.main.async {
            height = uiView.contentSize.height
        }
    }
    
    class Coordinator: NSObject, UITextViewDelegate {
        var parent: ExpandableInputField
        
        init(parentView: ExpandableInputField){
            parent = parentView
            
            super.init()
            
            NotificationCenter.default.addObserver(self, selector: #selector(clearText), name: Notification.Name("clear.expandable.input.field"), object: nil)
        }
        
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
        
        @objc private func clearText(){
            parent.text = ""
            print("DEBUG: called")
        }
        
        func textViewDidBeginEditing(_ textView: UITextView) {
            if parent.text.isEmpty {
                textView.text = ""
                textView.textColor = .black
            }
        }
        
        func textViewDidEndEditing(_ textView: UITextView) {
            textView.text = "Enter Message"
            textView.textColor = .gray
        }
        
        func textViewDidChange(_ textView: UITextView) {
            DispatchQueue.main.async {
                self.parent.height = textView.contentSize.height
                self.parent.text = textView.text
            }
        }
    }
}
