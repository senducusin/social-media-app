//
//  TweetActionViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/27/21.
//

import SwiftUI

class TweetActionViewModel: ObservableObject {
    let tweetVm: TweetViewModel
    var didLike = false {
        didSet {
            configureLikeButton()
        }
    }
    
    @Published var likeIconImage: String = "heart"
    @Published var likeIconColor: Color = .gray
    
    init(tweetVm: TweetViewModel){
        self.tweetVm = tweetVm
        checkLikeStatus()
    }
    
    private func configureLikeButton(){
        if didLike {
            likeIconImage = "heart.fill"
            likeIconColor = .red
        }else{
            likeIconImage = "heart"
            likeIconColor = .gray
        }
    }
    
    private func likeTweet(){
        TweetService.shared.like(tweetVm.tweet) { [weak self] error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            self?.didLike = true
        }
    }
    
    private func unlikeTweet(){
        TweetService.shared.unlike(tweetVm.tweet) { [weak self] error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            self?.didLike = false
        }
    }
}

extension TweetActionViewModel {

    func likeUnlikeHandler(){
        if didLike {
            unlikeTweet()
        }else{
            likeTweet()
        }
    }
    
    func checkLikeStatus(){
        TweetService.shared.checkIfUserLikedTweet(tweetVm.tweet) { [weak self] result in
            switch result {
            
            case .success(let didLike):
                self?.didLike = didLike
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
}
