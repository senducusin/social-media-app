//
//  ChatCellViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/24/21.
//

import SwiftUI

struct ChatCellViewModel {
    let messageVm: MessageViewModel
    let userVm: UserViewModel
    
    init(conversation: Conversation){
        messageVm = conversation.messageVm
        userVm = conversation.userVm
    }
    
    var isCurrentUser: Bool {
        userVm.isCurrentUser
    }
    
    var isRecepient: Bool {
        !userVm.isCurrentUser
    }
    
    var useCurrentUserBubble: Bool {
        isCurrentUser
    }
    
    var messageText: String{
        messageVm.text
    }
    
    var imageUrl: URL?{
        userVm.profileImageUrl
    }
    
    var backgroundColor: Color {
        isCurrentUser ?  .blue : Color(.systemGray5)
    }
    
    var foregroundColor: Color {
        isCurrentUser ?  .white : .black
    }
}
