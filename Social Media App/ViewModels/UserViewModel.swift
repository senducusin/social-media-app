//
//  UserViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/26/21.
//

import Foundation

struct UserViewModel: Identifiable {
    var user: SocialMediaUser
    
    var id: String {
        user.id
    }
    
    var username: String {
        user.username
    }
    
    var profileImageUrlString: String {
        user.profileImageUrl
    }
    
    var profileImageUrl: URL? {
        URL(string: user.profileImageUrl)
    }
    
    var fullname: String {
        user.fullname
    }
    
    var email: String {
        user.email
    }
    
    var description: String {
        user.description ?? ""
    }
    
    var isCurrentUser: Bool {
        guard let currentUid = AuthService.shared.currentUid else {return false}
        
        return currentUid == user.id
    }
    
    var followers: String {
        "\(user.stats.followers)"
    }
    
    var following: String {
        "\(user.stats.following)"
    }
}
