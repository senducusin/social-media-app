//
//  ChatViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/28/21.
//

import UIKit

class ChatViewModel: ObservableObject {
    var recepientUserVm: UserViewModel
    @Published var text = ""
    @Published var messages = [Conversation]()
    @Published var height: CGFloat = 0
    
    init(recepientUserVm: UserViewModel){
        self.recepientUserVm = recepientUserVm
        fetchMessages()
    }
    
    func fetchMessages(){
        MessageService.shared.fetchMessages(withUser: recepientUserVm.user) { [weak self] result in
            switch result {
            
            case .success(let conversation):
                if let messages = self?.messages {
                    var messages:[Conversation] = messages
                    messages.append(conversation)
                    messages.sort(by: {$0.messageVm.date < $1.messageVm.date})
                    self?.messages = messages
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func sendMessage(){
        guard let currentUserUid = AuthService.shared.currentUid else {return}
        
        let message = Message(text: text, fromId: currentUserUid, toId: recepientUserVm.id)
        
        text = ""
        NotificationCenter.default.post(name: Notification.Name("clear.expandable.input.field"), object: nil)
        
        MessageService.shared.sendMessage(message) {error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
        }
    }
}
