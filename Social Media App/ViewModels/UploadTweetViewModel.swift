//
//  UploadTweetViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/26/21.
//

import SwiftUI

class UploadTweetViewModel: ObservableObject {
    @Published var captionText = ""
    @Binding var isPresented: Bool
    
    init(isPresented: Binding<Bool>){
        self._isPresented = isPresented
    }
    
    func uploadTweet(){
        guard let userVm = AuthService.shared.userVm else {return}
        
        TweetService.shared.uploadBy(user: userVm.user, withCaption: captionText, likes: 0) { error in
            
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            self.isPresented = false
        }
    }
}
