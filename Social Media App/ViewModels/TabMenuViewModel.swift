//
//  ContentViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/31/21.
//

import SwiftUI

class TabMenuViewModel: ObservableObject {
    
    @Published var titleBar: String = "Home"
    
    @Published var selectedIndex = 0 {
        didSet{
            configure()
        }
    }
    
    private func configure(){
        titleBar = TabMenu(rawValue: selectedIndex)!.tabTitle
        print(titleBar)
    }
    
}

enum TabMenu: Int, CaseIterable {
    case home, search, messages
    
    var tabTitle: String {
        switch self {
        
        case .home:
            return "Home"
        case .search:
            return "Search"
        case .messages:
            return "Messages"
        }
    }
}
