//
//  FeedViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/27/21.
//

import Foundation

class FeedViewModel: ObservableObject {
    @Published var tweetVms  = [TweetViewModel]()
    
    init(){
        fetchTweets()
    }
    
    func fetchTweets(){
        
        TweetService.shared.fetchAll { [weak self] result in
            switch result {
            
            case .success(let tweets):
                self?.tweetVms = tweets.map({TweetViewModel(tweet: $0)})
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
    }
}
