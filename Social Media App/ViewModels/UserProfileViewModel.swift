//
//  UserProfileViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/26/21.
//

import SwiftUI

class UserProfileViewModel: ObservableObject {
    
    private var userTweets = [TweetViewModel]()
    private var likedTweets = [TweetViewModel]()
    
    @Published var isFollowed = false
    @Published var selectedFilter: ProfileFilterOptions = .tweets
    @Published var userVM: UserViewModel
    
    init(user: SocialMediaUser){
        self.userVM = UserViewModel(user: user)
        fetchUserStats()
        checkIfFollowed()
        fetchTweetsByUser()
        fetchTweetsByLike()
    }
}

extension UserProfileViewModel {
    var followLabel: String {
        isFollowed ? "Following" : "Follow"
    }
    
    var tweetsToShow: [TweetViewModel]{
        switch selectedFilter {
        
        case .tweets:
            return userTweets
        case .replies:
            return [TweetViewModel]()
        case .likes:
            return likedTweets
        }
    }
}

extension UserProfileViewModel {
    func followHandler(){
        if isFollowed {
            unfollow()
        }else{
            follow()
        }
    }
}

// MARK: - API
extension UserProfileViewModel {
    
    private func follow(){
        FollowService.shared.followUserBy(uid: userVM.id) { [weak self] error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            self?.isFollowed = true
            self?.userVM.user.stats.followers += 1
        }
    }
    
    private func unfollow(){
        FollowService.shared.unfollowUserBy(uid: userVM.id) { [weak self] error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            self?.isFollowed = false
            self?.userVM.user.stats.followers -= 1
        }
    }
    
    func fetchUserStats(){
        FollowService.shared.fetchUserStats(OfUser: userVM.user) { [weak self] result in
            
            switch result {
            case .success(let userStat):
                self?.userVM.user.stats = userStat
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func checkIfFollowed(){
        guard !userVM.isCurrentUser else {return}
        
        FollowService.shared.checkIfUserIsFollowed(uid: userVM.id) { [weak self] result in
            
            switch result {
            case .success(let isFollowed):
                self?.isFollowed = isFollowed
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func fetchTweetsByUser(){
        TweetService.shared.fetchAllBy(userVM.user) { [weak self] result in
           
            switch result {
            case .success(let tweets):
                self?.userTweets = tweets.map({ TweetViewModel(tweet: $0) })
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func fetchTweetsByLike(){
        TweetService.shared.fetchAllByLike(userVM.user) { [weak self] result in
            switch result {
            
            case .success(let tweets):
                self?.likedTweets = tweets.map(TweetViewModel.init)
            case .failure(let error):
                print("DEBUG: \(error.localizedDescription)")
            }
        }
    }
}
