//
//  TweetViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/27/21.
//

import Firebase

struct TweetViewModel: Identifiable {
    let tweet: Tweet
    
    var id: String {
        tweet.id
    }
    
    var caption: String {
        tweet.caption
    }
    
    var likes: Int {
        tweet.likes
    }
    
    var date: Date {
        tweet.timestamp.dateValue()
    }
    
    var timestamp: Timestamp? {
        tweet.timestamp
    }
    
    var uploader: SocialMediaUser {
        tweet.uploader
    }

}

extension TweetViewModel {
    var uploaderId: String {
        uploader.id
    }
    
    var uploaderUsername: String {
        uploader.username
    }
    
    var uploaderProfileImageUrlString: String {
        uploader.profileImageUrl
    }
    
    var uploaderProfileImageUrl: URL? {
        print(uploader.profileImageUrl)
        return URL(string: uploader.profileImageUrl)
    }
    
    var uploaderFullname: String {
        uploader.fullname
    }
    
    var uploaderEmail: String {
        uploader.email
    }
    
    var uploaderDescription: String {
        uploader.description ?? ""
    }
    
    var uploaderIsCurrentUser: Bool {
        guard let currentUid = AuthService.shared.currentUid else {return false}
        
        return currentUid == uploader.id
    }
    
    var timestampString: String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.second, .minute, .hour, .day, .weekOfMonth]
        formatter.maximumUnitCount = 1
        formatter.unitsStyle = .abbreviated
    
        return formatter.string(from: date, to: Date()) ?? ""
    }
    
    var detailedTimestampString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a • MM/dd/yyyy"
        return formatter.string(from: date)
    }
}
