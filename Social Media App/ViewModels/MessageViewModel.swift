//
//  MessageViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/28/21.
//

import Firebase

struct MessageViewModel: Identifiable {
    var message: Message
    
    var id: String {
        message.id.uuidString
    }
    
    var text: String {
        message.text
    }
    
    var fromId: String {
        message.fromId
    }
    
    var toId: String {
        message.toId
    }
    
    var timestamp: Timestamp {
        message.timestamp
    }
    
    var date: Date {
        message.timestamp.dateValue()
    }
    
    var isFromCurrentUser: Bool {
        guard let currentUid = AuthService.shared.currentUid else {return false}
        
        return currentUid == message.fromId
    }
    
    var chatPartnerId: String {
        isFromCurrentUser ? toId : fromId
    }
}
