//
//  RegistrationViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/25/21.
//

import SwiftUI

class RegistrationViewModel: ObservableObject {
    var email = ""
    var fullname = ""
    var username = ""
    var password = ""
    
    var showImagePicker = false
    var selectedUiimage: UIImage?
    var image: Image?
    
    
    func loadImage(){
        guard let selectedImage = selectedUiimage else {return}
        image = Image(uiImage: selectedImage)
    }
}
