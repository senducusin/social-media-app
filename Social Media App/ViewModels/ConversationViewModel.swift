//
//  ConversationViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/28/21.
//

import SwiftUI

class ConversationViewModel: ObservableObject {
    var recentMessagesDictionary = [String:Conversation]()
    @Published var recentMessages = [Conversation]()
    @Published var isShowingNewMessageView = false
    @Published var showChat = false
    @Published var userVm: UserViewModel?
    
    init(){
        fetchRecentMessages()
    }
    
    func fetchRecentMessages(){
        MessageService.shared.fetchRecentMessages { [weak self] result in
            
            switch result {
            case .success(let conversation):
                self?.recentMessagesDictionary[conversation.userVm.id] = conversation
                
                if let recentMessagesDictionary = self?.recentMessagesDictionary {
                    var recentMessagesArray = Array(recentMessagesDictionary.values)
                    recentMessagesArray.sort(by: {$0.messageVm.date > $1.messageVm.date})
                    
                    self?.recentMessages = recentMessagesArray
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
}
