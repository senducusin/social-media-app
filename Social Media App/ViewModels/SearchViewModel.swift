//
//  SearchViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/26/21.
//

import SwiftUI

enum SearchViewModelConfiguration{
    case search
    case newMessage
}

class SearchViewModel: ObservableObject {
    @Published var users = [SocialMediaUser]()
    @Published var searchText = ""
    
    
    init(config: SearchViewModelConfiguration){
        fetchUsers(config: config)
    }
    
    var showUsers:[SocialMediaUser]{
        searchText.isEmpty ? users : filteredUsers()
    }
    
    func fetchUsers(config: SearchViewModelConfiguration){
        guard let currentUid = AuthService.shared.currentUid else {return}
        
        UserService.shared.fetchUsers { [weak self] result in
            
            switch result{
            case .success(let users):
                
                switch config {
                case .search:
                    self?.users = users
                case .newMessage:
                    self?.users = users.filter({$0.id != currentUid})
                }
               
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func filteredUsers() -> [SocialMediaUser] {
        let lowercasedQuery = searchText.lowercased()
        return users.filter({
            $0.fullname.lowercased().contains(lowercasedQuery) ||
            $0.username.lowercased().contains(lowercasedQuery)
        })
    }
}
