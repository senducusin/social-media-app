//
//  AuthViewModel.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/25/21.
//

import SwiftUI
import Firebase

class AuthViewModel: ObservableObject {
    @Published var userSession: FirebaseAuth.User?
    @Published var isAuthenticating = false
    @Published var error: Error?
    @Published var user: UserViewModel?
    
    init(){
        userSession = Auth.auth().currentUser
        fetchUser()
    }
    
    func login(withEmail email: String, password: String) {
        AuthService.shared.signIn(withEmail: email, password: password) { [weak self] result in
            
            switch result {
            case .success(let user):
                self?.userSession = user
                self?.fetchUser()
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func registerUser(email:String, password: String, username: String, fullname: String, profileImage: UIImage?){
        
        guard let image = profileImage else {return}
        
        let newUser = NewUser(email: email, password: password, username: username, fullname: fullname)
        
        AuthService.shared.register(newUser: newUser, profileImage: image) { [weak self] result in
            
            switch result {
            case .success(let user):
                self?.userSession = user
                self?.fetchUser()
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
    
    func signOut(){
        AuthService.shared.signOut { [weak self] error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            self?.userSession = nil
        }
    }
    
    func fetchUser(){
        UserService.shared.fetchUser { result in
            
            switch result {
            case .success(let user):
                self.user = UserViewModel(user: user)
            case .failure(let error):
                print(error.localizedDescription)
            }
            
        }
    }
}
