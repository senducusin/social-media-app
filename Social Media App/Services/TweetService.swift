//
//  TweetService.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/26/21.
//

import Firebase

enum TweetServiceError:Error {
    case encodingError
    case snapshotError
    case notLoggedInError
}

class TweetService {
    static let shared = TweetService()
    
    private let tweetsRef = Firestore.firestore().collection("tweets")
    
    private let keyTweetLikes = "tweet-likes"
    private let keyUserLikes = "user-likes"
}

extension TweetService {
    
    func checkIfUserLikedTweet(_ tweet: Tweet, completion:@escaping(Result<Bool, Error>)->()){
        guard let uid = AuthService.shared.currentUid else {
            completion(.failure(TweetServiceError.notLoggedInError))
            return
        }
        
        let userLikesRef = tweetsRef.document(uid).collection(keyUserLikes)
        
        userLikesRef.document(tweet.id).getDocument { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let didLike = snapshot?.exists else {
                completion(.failure(TweetServiceError.snapshotError))
                return
            }
            completion(.success(didLike))
        }
    }
    
    func like(_ tweet: Tweet, completion:@escaping(Error?)->()){
        guard let uid = AuthService.shared.currentUid else {
            completion(TweetServiceError.notLoggedInError)
            return
        }
        
        let tweetLikesRef = tweetsRef.document(tweet.id).collection(keyTweetLikes)
        let userLikesRef = tweetsRef.document(uid).collection(keyUserLikes)
        
        tweetsRef.document(tweet.id).updateData(["likes":tweet.likes + 1]) { error in
            if let error = error {
                completion(error)
                return
            }
            
            tweetLikesRef.document(uid).setData(([:])) { error in
                if let error = error {
                    completion(error)
                    return
                }
                
                userLikesRef.document(tweet.id).setData([:]) { error in
                    if let error = error {
                        completion(error)
                        return
                    }
                    
                    completion(nil)
                }
            }
        }
        
    }
    
    func unlike(_ tweet: Tweet, completion:@escaping(Error?)->()){
        guard let uid = AuthService.shared.currentUid else {
            completion(TweetServiceError.notLoggedInError)
            return
        }
        
        let tweetLikesRef = tweetsRef.document(tweet.id).collection(keyTweetLikes)
        let userLikesRef = tweetsRef.document(uid).collection(keyUserLikes)
        
        tweetsRef.document(tweet.id).updateData(["likes":tweet.likes - 1]) { error in
            if let error = error {
                completion(error)
                return
            }
            
            tweetLikesRef.document(uid).delete { error in
                if let error = error {
                    completion(error)
                    return
                }
                
                userLikesRef.document(tweet.id).delete { error in
                    if let error = error {
                        completion(error)
                        return
                    }
                    
                    completion(nil)
                }
            }
        }
    }
    
    func fetchAll(completion:@escaping(Result<[Tweet], Error>)->()){
        tweetsRef.getDocuments { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let documents = snapshot?.documents else {
                completion(.failure(TweetServiceError.snapshotError))
                return
            }
            
            var tweets = [Tweet]()
            
            documents.forEach { document in
                if let tweet = Tweet(document.data()) {
                    tweets.append(tweet)
                }
            }
            
            completion(.success(tweets))
        }
    }
    
    func fetchAllBy(_ user: SocialMediaUser, completion:@escaping(Result<[Tweet],Error>)->()){
        
        tweetsRef.whereField("uploader.id", isEqualTo: user.id).getDocuments { snapshot, error in
            
            guard let documents = snapshot?.documents else {
                completion(.failure(TweetServiceError.snapshotError))
                return
            }
            
            var tweets = [Tweet]()
            
            documents.forEach { document in
                if let tweet = Tweet(document.data()) {
                    tweets.append(tweet)
                }
            }
            
            completion(.success(tweets))
        }
        
    }
    
    func fetchAllByLike(_ user: SocialMediaUser, completion:@escaping(Result<[Tweet], Error>)->()){
        
        var tweets = [Tweet]()
        var tweetsFetchedCounter = 0
        
        tweetsRef.document(user.id).collection(keyUserLikes).getDocuments { [weak self] snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let documents = snapshot?.documents else {
                completion(.failure(TweetServiceError.snapshotError))
                return
            }
            
            let tweetIds = documents.map({ $0.documentID })
            
            tweetIds.forEach { [weak self] tweetId in
                self?.tweetsRef.document(tweetId).getDocument { snapshot, error in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    
                    guard let data = snapshot?.data() else {
                        completion(.failure(TweetServiceError.snapshotError))
                        return
                    }
                    
                    tweetsFetchedCounter += 1
                    
                    if let tweet = Tweet(data) {
                        tweets.append(tweet)
                    }
                    
                    guard tweetsFetchedCounter == tweetIds.count else {return}
                    completion(.success(tweets))
                    
                }
            }
        }
        
    }
    
    func uploadBy(user: SocialMediaUser, withCaption caption: String, likes:Int, completion: @escaping(Error?)->()){
        
        let tweetDocRef = tweetsRef.document()
        
        let tweet = Tweet(id: tweetDocRef.documentID, uploader: user, caption: caption, likes: likes)
        
        guard let data = tweet.toFirebaseDictionary() else {
            completion(TweetServiceError.encodingError)
            return
        }
        
        tweetDocRef.setData(data) { error in
            if let error = error {
                completion(error)
                return
            }
            
            completion(nil)
        }
    }
}
