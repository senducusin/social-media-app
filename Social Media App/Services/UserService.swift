//
//  UserService.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/25/21.
//

import Firebase

enum UserServiceError:Error {
    case documentsNotFoundError
}

class UserService {
    static let shared = UserService()
    
    let usersRef = Firestore.firestore().collection("users")
    
    private func setFetchedUserAsCurrentUser(_ user: SocialMediaUser){
        if let currentUserUid = Auth.auth().currentUser?.uid,
           currentUserUid == user.id{
            let userVm = UserViewModel(user: user)
            AuthService.shared.userVm = userVm
        }
    }
}

extension UserService {
    func fetchUser(completion:@escaping((Result<SocialMediaUser, Error>) ->())){
        guard let userSession = Auth.auth().currentUser else {return}
        
        usersRef.document(userSession.uid).getDocument { [weak self] snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = snapshot?.data() else {return}
            let user = SocialMediaUser(firebaseDictionary: data)
            
            self?.setFetchedUserAsCurrentUser(user)
            
            completion(.success(user))
        }
    }
    
    func fetchUsers(completion:@escaping(Result<[SocialMediaUser],Error>)->()){
        usersRef.getDocuments { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let documents = snapshot?.documents else {
                completion(.failure(UserServiceError.documentsNotFoundError))
                return
            }
            
            let users = documents.map({SocialMediaUser(firebaseDictionary: $0.data())})
            completion(.success(users))
        }
    }
}
