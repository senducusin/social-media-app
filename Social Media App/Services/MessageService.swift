//
//  MessageService.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/28/21.
//

import Firebase

enum MessageServiceError: Error{
    case notLoggedInError
    case encodingError
    case snapshotError
}

class MessageService {
    static let shared = MessageService()
    
    let messagesRef = Firestore.firestore().collection("messages")
    let usersRef = Firestore.firestore().collection("users")
    let keyRecentMessage = "recent-messages"
}

// MARK: - API
extension MessageService {
    func fetchRecentMessages(completion:@escaping(Result<Conversation, Error>)->()){
        guard let currentUid = AuthService.shared.currentUid else {
            completion(.failure(MessageServiceError.notLoggedInError))
            return
        }
        
        let query = messagesRef.document(currentUid).collection(keyRecentMessage)
//        query.order(by: "timestamp", descending: true)
        
        query.addSnapshotListener { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let changes = snapshot?.documentChanges else {
                completion(.failure(MessageServiceError.snapshotError))
                return
            }
            
            changes.forEach { change in
                let messageData = change.document.data()
                let uid = change.document.documentID
                
                self.usersRef.document(uid).getDocument {snapshot, error in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    
                    guard let data = snapshot?.data() else {
                        completion(.failure(MessageServiceError.snapshotError))
                        return
                    }
                    
                    let user = SocialMediaUser(firebaseDictionary: data)
                    let message = Message(dict: messageData)
                    
                    completion(.success(Conversation(message: message, user: user)))
                }
            }
        }
    }
    
    func fetchMessages(withUser user: SocialMediaUser, completion:@escaping(Result<Conversation, Error>)->()){
        guard let currentUid = AuthService.shared.currentUid else {
            completion(.failure(MessageServiceError.notLoggedInError))
            return
        }
        
        let query = messagesRef.document(currentUid).collection(user.id)

        
        query.addSnapshotListener { [weak self] snapshot, error in
            guard let changes = snapshot?.documentChanges.filter({$0.type == .added}) else {
                print("DEBUG: document changed here")
                return
            }
            
            
            changes.forEach { change in
                let messageData = change.document.data()
                
                guard let fromId = messageData["fromId"] as? String else {return}
                
                self?.usersRef.document(fromId).getDocument(completion: { snapshot, error in
                    if let error = error {
                        completion(.failure(error))
                        return
                    }
                    
                    guard let data = snapshot?.data() else {
                        completion(.failure(MessageServiceError.snapshotError))
                        return
                    }
                    
                    let user = SocialMediaUser(firebaseDictionary: data)
                    let message = Message(dict: messageData)
                    
                    completion(.success(Conversation(message: message, user: user)))
                })
            }
            
        }
    }
    
    func sendMessage(_ message: Message, completion:@escaping(Error?)->()){
        
        let currentUserRef = messagesRef.document(message.fromId).collection(message.toId).document()
        let recepientUserRef = messagesRef.document(message.toId).collection(message.fromId)
        
        let curretRecentRef = messagesRef.document(message.fromId).collection(keyRecentMessage)
        let recepientRecentRef = messagesRef.document(message.toId).collection(keyRecentMessage)
        
        let messageId = currentUserRef.documentID
        
        let messageData = message.toFirebaseDictionary()
        
        currentUserRef.setData(messageData) { error in
            if let error = error {
                completion(error)
                return
            }
            
            recepientUserRef.document(messageId).setData(messageData) { error in
                if let error = error {
                    completion(error)
                    return
                }
                
                curretRecentRef.document(message.toId).setData(messageData) { error in
                    if let error = error {
                        completion(error)
                        return
                    }
                    
                    recepientRecentRef.document(message.fromId).setData(messageData) { error in
                        if let error = error {
                            completion(error)
                            return
                        }
                        
                        completion(nil)
                    }
                }
            }
        }
    }
    
}
