//
//  FollowService.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/26/21.
//

import Firebase

enum FollowServiceError:Error {
    case notLoggedInError
    case missingSnapshotError
}

class FollowService {
    static let shared = FollowService()
    
    let followersRef = Firestore.firestore().collection("followers")
    let followingRef = Firestore.firestore().collection("following")
    
    let followingKey = "user-following"
    let followersKey = "user-followers"
}

extension FollowService {
    func fetchUserStats(OfUser user:SocialMediaUser, completion:@escaping(Result<UserStat, Error>)->()){
        
        let refUserFollowers = followersRef.document(user.id).collection(followersKey)
        let refUserFollowing = followingRef.document(user.id).collection(followingKey)
        
        refUserFollowers.getDocuments { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let followerCount = snapshot?.documents.count else {
                completion(.failure(FollowServiceError.missingSnapshotError))
                return
            }
            
            refUserFollowing.getDocuments { snapshot, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let followingCount = snapshot?.documents.count else {
                    completion(.failure(FollowServiceError.missingSnapshotError))
                    return
                }
                
                let stats = UserStat(followers: followerCount, following: followingCount)
                
                completion(.success(stats))
                
            }
        }
        
    }
    
    func followUserBy(uid: String, completion:@escaping(Error?)->()){
        guard let currentUid = Auth.auth().currentUser?.uid else {
            completion(FollowServiceError.notLoggedInError)
            return
        }
        
        followingRef.document(currentUid).collection(followingKey).document(uid).setData([:]) { [weak self] error in
            if let error = error {
                completion(error)
                return
            }
            
            self?.followersRef.document(uid).collection(self!.followersKey).document(currentUid).setData([:]) { error in
                if let error = error {
                    completion(error)
                    return
                }
                
                completion(nil)
            }
        }
    }
    
    func unfollowUserBy(uid: String, completion:@escaping(Error?)->()){
        guard let currentUid = Auth.auth().currentUser?.uid else {
            completion(FollowServiceError.notLoggedInError)
            return
        }
        
        followingRef.document(currentUid).collection(followingKey).document(uid).delete { [weak self] error in
            if let error = error {
                completion(error)
                return
            }
            
            self?.followersRef.document(uid).collection(self!.followersKey).document(currentUid).delete { error in
                if let error = error {
                    completion(error)
                    return
                }
                
                completion(nil)
            }
        }
    }
    
    func checkIfUserIsFollowed(uid: String, completion:@escaping(Result<Bool, Error>)->()){
        guard let currentUid = Auth.auth().currentUser?.uid else {
            completion(.failure(FollowServiceError.notLoggedInError))
            return
        }
        
        followingRef.document(currentUid).collection(followingKey).document(uid).getDocument { snapshot, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let isFollowed = snapshot?.exists else {
                completion(.failure(FollowServiceError.missingSnapshotError))
                return
            }
            
            completion(.success(isFollowed))
        }
    }
}
