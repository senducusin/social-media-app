//
//  AuthService.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/25/21.
//

import UIKit
import Firebase

enum AuthError:Error {
    case dataConversionError
    case urlNotFoundError
    case invalidUserError
    case passwordNotFoundError
    case updateError
}

class AuthService {
    
    static let shared = AuthService()
    
    var userVm: UserViewModel?
    
    var currentUid: String? {
        Auth.auth().currentUser?.uid
    }
    
    func signIn(withEmail email: String, password: String, completion:@escaping(Result<User,Error>)->()) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            if let user = result?.user {
                completion(.success(user))
            }
        }
    }
    
    func signOut(completion:@escaping(Error?)->()){
        do {
            try Auth.auth().signOut()
            userVm = nil
            completion(nil)
        } catch {
            completion(error)
        }
    }
    
    func register(newUser: NewUser, profileImage: UIImage, completion:@escaping(Result<User,Error>)->()){
        uploadProfileImage(profileImage) { [weak self] result in
            
            switch result {
            case .success(let profileImageUrlString):
                
                let newUser = NewUser(newUser, uid: nil, profileImageUrl: profileImageUrlString)
                
                self?.createUser(newUser, completion: { result in
                    switch result {
                    
                    case .success(let user):
                        completion(.success(user))
                    case .failure(let error):
                        completion(.failure(error))
                        print("DEBUG: Error in creating user")
                    }
                })
                
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }
    
}

extension AuthService {
    private func createUser(_ newUser: NewUser, completion:@escaping(Result<User, Error>) -> ()){
        
        guard let password = newUser.password else {
            completion(.failure(AuthError.passwordNotFoundError))
            return
        }
        
        Auth.auth().createUser(withEmail: newUser.email, password: password) { [weak self] auth, error in
            
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let user = auth?.user else {
                completion(.failure(AuthError.invalidUserError))
                return
                
            }
            
            let newUser = NewUser(newUser, uid: user.uid)
            self?.updateUserInformation(withUser: newUser) { error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                completion(.success(user))
            }
        }
    }
    
    private func updateUserInformation(withUser newUser:NewUser, completion:@escaping(Error?) -> ()){
        
        guard let userDictionary = newUser.toDictionary(),
              let uid = newUser.uid else {
            completion(AuthError.updateError)
            return
        }
        
        Firestore.firestore().collection("users").document(uid).setData(userDictionary) { error in
            if let error = error {
                completion(error)
                return
            }
            
            completion(nil)
        }
    }
    
    private func uploadProfileImage(_ image: UIImage, completion:@escaping(Result<String, Error>)->()){
        guard let imageData = image.jpegData(compressionQuality: 0.3) else {
            completion(.failure(AuthError.dataConversionError))
            return
        }
        
        let filename = NSUUID().uuidString
        let storageRef = Storage.storage().reference().child(filename)
        
        storageRef.putData(imageData, metadata: nil) { _, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            storageRef.downloadURL { url, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let profileImageUrl = url?.absoluteString else {
                    completion(.failure(AuthError.urlNotFoundError))
                    return
                }
                
                completion(.success(profileImageUrl))
            }
        }
    }
}
