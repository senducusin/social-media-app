//
//  Social_Media_AppApp.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/21/21.
//

import SwiftUI
import Firebase

@main
struct Social_Media_AppApp: App {
    
    init(){
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(AuthViewModel())
        }
    }
}
