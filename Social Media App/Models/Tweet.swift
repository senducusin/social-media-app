//
//  Tweet.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/26/21.
//

import Firebase

struct Tweet: Identifiable {
    var id: String
    var uploader: SocialMediaUser
    var caption: String
    var likes: Int
    var timestamp = Timestamp(date: Date())
}

extension Tweet {
    init?(_ firebaseDictionary: [String:Any]){
        guard let id = firebaseDictionary["id"] as? String,
              let caption = firebaseDictionary["caption"] as? String,
              let likes = firebaseDictionary["likes"] as? Int,
              let userDictionary = firebaseDictionary["uploader"] as? [String:Any],
              let timestamp = firebaseDictionary["timestamp"] as? Timestamp else{
            print("DEBUG: error parsing")
            return nil
        }
        
        self.id = id
        self.uploader = SocialMediaUser(firebaseDictionary: userDictionary)
        self.caption = caption
        self.likes = likes
        self.timestamp = timestamp
    }
    
    func toFirebaseDictionary() -> [String: Any]?{
        
        guard let uploaderDictionary = uploader.toDictionary() else {return nil}
        
        return [
            "id":id,
            "uploader":uploaderDictionary,
            "caption": caption,
            "likes": likes,
            "timestamp":timestamp
        ]
    }
}
