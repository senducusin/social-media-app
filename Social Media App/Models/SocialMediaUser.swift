//
//  SocialMediaUser.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/25/21.
//

import Foundation

struct SocialMediaUser: Identifiable, Codable{
    let id: String
    let username: String
    let profileImageUrl: String
    let fullname: String
    let email: String
    let description: String?
    var stats: UserStat
}

struct UserStat: Codable {
    var followers: Int
    var following: Int
}

extension SocialMediaUser {
    init(firebaseDictionary: [String:Any]){
        self.id = firebaseDictionary["uid"] as? String ?? ""
        self.username = firebaseDictionary["username"] as? String ?? ""
        self.profileImageUrl = firebaseDictionary["profileImageUrl"] as? String ?? ""
        self.fullname = firebaseDictionary["fullname"] as? String ?? ""
        self.email = firebaseDictionary["email"] as? String ?? ""
        self.description = firebaseDictionary["description"] as? String
        
        var followers: Int? = nil
        var following: Int? = nil
        
        if let stats = firebaseDictionary["stats"] as? [String:Any] {
            followers = stats["followers"] as? Int
            following = stats["following"] as? Int
        }
        
        self.stats = UserStat(followers: followers ?? 0, following: following ?? 0)

    }
}
