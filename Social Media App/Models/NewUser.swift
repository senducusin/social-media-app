//
//  NewUser.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/25/21.
//

import Foundation

struct NewUser: Codable {
    var uid: String?
    var email: String
    var password: String?
    var username: String
    var fullname: String
    var profileImageUrl: String?
}

extension NewUser {
    init(_ newUser: NewUser, uid: String?, profileImageUrl: String){
        self.uid = uid
        self.email = newUser.email
        self.password = newUser.password
        self.username = newUser.username
        self.fullname = newUser.fullname
        self.profileImageUrl = profileImageUrl
    }
}

extension NewUser {
    init(_ newUser: NewUser, uid: String){
        self.uid = uid
        self.email = newUser.email
        self.password = nil
        self.username = newUser.username
        self.fullname = newUser.fullname
        self.profileImageUrl = newUser.profileImageUrl
    }
}
