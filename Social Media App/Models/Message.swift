//
//  Message.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/22/21.
//

import Firebase

struct Message : Identifiable {
    var id = UUID()
    let text: String
    let fromId: String
    let toId: String
    var timestamp = Timestamp(date: Date())
}

extension Message {
    func toFirebaseDictionary() -> jsonDictionary {
        return [
            "id":id.uuidString,
            "text":text,
            "fromId":fromId,
            "toId":toId,
            "timestamp":timestamp
        ]
    }
    
    init(dict: jsonDictionary){
        
        if let idString = dict["id"] as? String,
           let uuid = UUID(uuidString: idString){
            self.id = uuid
        }else{
            self.id = UUID()
        }
        text = dict["text"] as? String ?? ""
        fromId = dict["fromId"] as? String ?? ""
        toId = dict["toId"] as? String ?? ""
        timestamp = dict["timestamp"] as? Timestamp ?? Timestamp(date: Date())
    }
}

struct Conversation: Identifiable {
    var id = UUID()
    var message: Message
    var user: SocialMediaUser
    
    var userVm: UserViewModel {
        UserViewModel(user: user)
    }
    
    var messageVm: MessageViewModel {
        MessageViewModel(message: message)
    }
}


// Draft
struct MockMessage: Identifiable {
    let id = UUID()
    let imageName: String
    let messageText: String
    let isCurrentUser: Bool
}

extension MockMessage {
    static let samples = [
        MockMessage(imageName: "spiderman", messageText: "Hey what's up?", isCurrentUser: false),
        MockMessage(imageName: "batman", messageText: "I'm Batman", isCurrentUser: true),
        MockMessage(imageName: "spiderman", messageText: "Blech", isCurrentUser: false),
        MockMessage(imageName: "batman", messageText: "m,dsnf,smfnsm,nmns,dmnf", isCurrentUser: true)
    ]
}
