//
//  LazyView.swift
//  Social Media App
//
//  Created by Jansen Ducusin on 5/28/21.
//

import SwiftUI

struct LazyView<Content: View>: View {
    let build: () -> Content
    
    init(_ build: @autoclosure @escaping()-> Content){
        self.build = build
    }
    
    var body: Content {
        build()
    }
}
